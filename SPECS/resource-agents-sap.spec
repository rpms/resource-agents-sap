#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#

# Below is the script used to generate a new source file
# from the resource-agent upstream git repo.
#
# TAG=$(git log --pretty="format:%h" -n 1)
# distdir="ClusterLabs-resource-agents-${TAG}"
# TARFILE="${distdir}.tar.gz"
# rm -rf $TARFILE $distdir
# git archive --prefix=$distdir/ HEAD | gzip > $TARFILE
#

%global upstream_prefix ClusterLabs-resource-agents
%global upstream_version e711383f

%global sap_script_prefix sap_cluster_connector
%global sap_script_hash 0015fe2

Name:		resource-agents-sap
Summary:	SAP cluster resource agents
Version:	4.1.1
Release:	34%{?rcver:%{rcver}}%{?numcomm:.%{numcomm}}%{?alphatag:.%{alphatag}}%{?dirty:.%{dirty}}%{?dist}
License:	GPLv2+
URL:		https://github.com/ClusterLabs/resource-agents
%if 0%{?fedora} || 0%{?centos_version} || 0%{?rhel}
Group:		System Environment/Base
%else
Group:		Productivity/Clustering/HA
%endif
Source0:	%{upstream_prefix}-%{upstream_version}.tar.gz
Source1:	%{sap_script_prefix}-%{sap_script_hash}.tar.gz
Patch0: 	bz1751949-1-SAPInstance-add-reload-action.patch
Patch1: 	bz1751949-2-SAPInstance-improve-profile-detection.patch
Patch2: 	bz1751949-3-SAPInstance-metadata-improvements.patch
Patch3: 	bz1817439-use-safe-temp-file-location.patch
Patch4: 	bz2024140-SAPInstance-1-add-systemd-support.patch
Patch5: 	bz2024140-SAPInstance-2-fix-systemd-issue.patch
Patch6: 	bz1937026-SAPDatabase-SAPInstance-set-correct-agent-ocf-version.patch
Patch7:   bz2151296-SAPInstance-improve-killsap-logic.patch

BuildArch:	noarch

BuildRequires: automake autoconf gcc
BuildRequires: perl-interpreter python3-devel
BuildRequires: libxslt glib2-devel

%if 0%{?fedora} || 0%{?centos_version} || 0%{?rhel}
BuildRequires: docbook-style-xsl docbook-dtds
%endif

Requires:	resource-agents >= 4.1.1

Requires:	/bin/bash /usr/bin/grep /bin/sed /bin/gawk

%description
The SAP resource agents interface with Pacemaker to allow
SAP instances to be managed in a cluster environment.

%prep
%setup -q -n %{upstream_prefix}-%{upstream_version}
%setup -q -T -D -a 1 -n %{upstream_prefix}-%{upstream_version}
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1

%build
if [ ! -f configure ]; then
	./autogen.sh
fi

%global rasset linux-ha

%configure BASH_SHELL="/bin/bash" \
	PYTHON="%{__python3}" \
	%{conf_opt_fatal} \
%if %{defined _unitdir}
    --with-systemdsystemunitdir=%{_unitdir} \
%endif
%if %{defined _tmpfilesdir}
    --with-systemdtmpfilesdir=%{_tmpfilesdir} \
    --with-rsctmpdir=/run/resource-agents \
%endif
	--with-pkg-name=%{name} \
	--with-ras-set=%{rasset}

%if %{defined jobs}
JFLAGS="$(echo '-j%{jobs}')"
%else
JFLAGS="$(echo '%{_smp_mflags}')"
%endif

make $JFLAGS

%install
make install DESTDIR=%{buildroot}

# remove other agents
find %{buildroot}/usr/lib/ocf ! -type d ! -iname "SAP*" -exec rm {} \;
find %{buildroot}/%{_mandir} -type f ! -iname "*SAP*" -exec rm {} \;

cp %{sap_script_prefix}-%{sap_script_hash}/redhat/sap_redhat_cluster_connector %{buildroot}/%{_sbindir}/sap_redhat_cluster_connector

## tree fixup
# remove docs (there is only one and they should come from doc sections in files)
rm -rf %{buildroot}/usr/share/doc/resource-agents

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_usr}/lib/ocf/resource.d/heartbeat/SAP*
%{_usr}/lib/ocf/lib/heartbeat/sap*
%{_mandir}/man7/*SAP*
%{_sbindir}/sap_redhat_cluster_connector

%exclude /etc
%exclude /usr/include
%exclude %{_usr}/lib/debug
%exclude %{_usr}/lib/systemd
%exclude %{_usr}/lib/tmpfiles.d
%exclude /usr/libexec/heartbeat
%exclude /usr/sbin/ldirectord
%exclude /usr/sbin/ocf*
%exclude /usr/share/%{name}
%exclude /usr/src

%changelog
* Wed Dec 07 2022 Janine Fuchs <jfuchs@redhat.com> - 4.1.1-34
- SAPInstance: be more resilient against broken kill.sap files

  Resolves: rhbz#2151296

* Fri Jun 17 2022 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.1.1-32
- SAPDatabase/SAPInstance: set correct agent and OCF version in metadata

  Resolves: rhbz#1937026

* Thu Nov 18 2021 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.1.1-31
- SAPInstance: add systemd support

  Resolves: rhbz#2024140

* Fri Apr 17 2020 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.1.1-30
- use safe temp file location

  Resolves: rhbz#1817439

* Thu Jan 23 2020 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.1.1-29
- SAPInstance: add reload-action

  Resolves: rhbz#1751949

* Tue Jun 18 2019 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.1.1-28
- Initial build as separate package

  Resolves: rhbz#1688341
